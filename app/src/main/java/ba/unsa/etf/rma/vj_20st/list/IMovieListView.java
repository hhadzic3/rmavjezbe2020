package ba.unsa.etf.rma.vj_20st.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_20st.data.Movie;

public interface IMovieListView {
    void setMovies(ArrayList<Movie> movies);
    void notifyMovieListDataSetChanged();
    void showToast(String t);
}
