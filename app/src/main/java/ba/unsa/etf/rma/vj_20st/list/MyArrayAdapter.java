package ba.unsa.etf.rma.vj_20st.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;
import ba.unsa.etf.rma.vj_20st.R;
import ba.unsa.etf.rma.vj_20st.data.Movie;

public class MyArrayAdapter extends ArrayAdapter<Movie> {
    int resource;
    private String posterPath="https://image.tmdb.org/t/p/w342";
    private Movie m;

    public MyArrayAdapter(Context context, int _resource, List<Movie> items) {
        super(context, _resource, items);
        resource = _resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout newView;

        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        }
        else newView = (LinearLayout)convertView;

        m = getItem(position);

        ImageView img = (ImageView) newView.findViewById(R.id.icon);
        TextView name = (TextView) newView.findViewById(R.id.ItemName);
        TextView genre = (TextView) newView.findViewById(R.id.ItemGenre);
/*
        if (m.getGenre().equals("Action"))
        img.setImageResource(R.drawable.action_movie);
        else if (m.getGenre().equals("Fantasy"))
            img.setImageResource(R.drawable.science_movie);
        else if (m.getGenre().equals("Comedy"))
            img.setImageResource(R.drawable.comedy_movie);

 */
        name.setText(m.getTitle());
        genre.setText(m.getReleaseDate());

        Glide.with(getContext())
                .load(posterPath+m.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.action_movie)
                .error(R.drawable.action_movie)
                .fallback(R.drawable.action_movie)
                .into(img);

        return newView;
    }
    public void setMovies(ArrayList<Movie> movies) {
        this.addAll(movies);
    }






}
