package ba.unsa.etf.rma.vj_20st.list;

interface IMovieListPresenter {

    void refreshMovies();

    void searchMovies(String toString);
}
