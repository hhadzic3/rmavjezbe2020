package ba.unsa.etf.rma.vj_20st.detail;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.fragment.app.Fragment;

import ba.unsa.etf.rma.vj_20st.R;
import ba.unsa.etf.rma.vj_20st.data.Movie;

public class MovieDetailFragment extends Fragment  {
    private TextView title;
    private TextView textGenre;
    private TextView textDate;
    private TextView textDescription;
    private TextView textLink;
    private Button shere;
    private Movie movie;
    private ToggleButton toggleButton;

    private CompoundButton.OnCheckedChangeListener toggleOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("similar", movie.getSimelars());
                SimilarFragment similarFragment = new SimilarFragment();
                similarFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frameList, similarFragment).commit();
            } else {
                Bundle arguments = new Bundle();
                arguments.putStringArrayList("cast", movie.getActors());
                CastFragment castFragment = new CastFragment();
                castFragment.setArguments(arguments);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.frameList, castFragment).commit();
            }
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)    {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        if (getArguments() != null && getArguments().containsKey("movie")) {

            movie = getArguments().getParcelable("movie");

            toggleButton = (ToggleButton) view.findViewById(R.id.toggle_button);

            title = (TextView) view.findViewById(R.id.title);

            textGenre = (TextView) view.findViewById(R.id.genre);
            textDate = (TextView) view.findViewById(R.id.date);
            textDescription = (TextView) view.findViewById(R.id.description);
            textLink = (TextView) view.findViewById(R.id.link);
            shere = (Button) view.findViewById(R.id.shere);

            ImageView img = (ImageView) view.findViewById(R.id.imageTitle);

            title.setText(movie.getTitle());
            textGenre.setText(movie.getGenre());
            textDate.setText(movie.getReleaseDate());
            textDescription.setText(movie.getOverview());
            textLink.setText(movie.getHomepage());

            if (textGenre.getText() == "Action" )
                img.setImageResource(R.drawable.action_movie);
            else if (textGenre.getText().equals("Fantasy"))
                img.setImageResource(R.drawable.science_movie);
            else if (textGenre.getText().equals("Comedy"))
                img.setImageResource(R.drawable.comedy_movie);

            Bundle arguments = new Bundle();
            arguments.putStringArrayList("cast", movie.getActors());
            CastFragment castFragment = new CastFragment();
            castFragment.setArguments(arguments);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.frameList, castFragment)
                    .commit();

            textLink.setOnClickListener(homepageOnClickListener);
            title.setOnClickListener(titleOnClickListener);
            shere.setOnClickListener(shareOnClickListener);
            toggleButton.setOnCheckedChangeListener(toggleOnCheckedChangeListener);
        }
        return view;
    }

    private View.OnClickListener homepageOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("http://" + movie.getHomepage()));
            startActivity(i);
        }
    };

    private View.OnClickListener titleOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://www.youtube.com/results?search_query=" + movie.getTitle() + " trailer"));
            startActivity(intent);
        }
    };

    private View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Kreiranje tekstualne poruke
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, movie.getOverview());
            sendIntent.setType("text/plain");
            // Provjera da li postoji aplikacija koja moze obaviti navedenu akciju
            if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(sendIntent);
            }
        }
    };



}