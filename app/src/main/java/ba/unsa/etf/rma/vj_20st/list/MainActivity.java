package ba.unsa.etf.rma.vj_20st.list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.FrameLayout;
import java.util.Locale;
import ba.unsa.etf.rma.vj_20st.MyBroadcastReceiver;
import ba.unsa.etf.rma.vj_20st.R;
import ba.unsa.etf.rma.vj_20st.data.Movie;
import ba.unsa.etf.rma.vj_20st.detail.MovieDetailFragment;

public class MainActivity extends AppCompatActivity implements MovieListFragment.OnItemClick{

    private MyBroadcastReceiver receiver = new MyBroadcastReceiver();
    // twoPaneMode ako je true radi se o sirem layoutu (dva fragmenta)
    // ako je twoPaneMode false tada se radi o pocetnom layoutu (jedan fragment)
    private boolean twoPaneMode=false;
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Locale locale = new Locale("bs");
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration , getBaseContext().getResources().getDisplayMetrics());


        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.movie_detail);

        //slucaj layouta za siroke ekrane
        if (details != null) {
            twoPaneMode = true;
            MovieDetailFragment detailFragment = (MovieDetailFragment) fragmentManager.findFragmentById(R.id.movie_detail);
            if (detailFragment==null) {
                detailFragment = new MovieDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.movie_detail,detailFragment).commit();
            }
        }
        else twoPaneMode = false;

        Fragment listFragment = fragmentManager.findFragmentByTag("ba/unsa/etf/rma/vj_20st/list");
        if (listFragment==null){
            listFragment = new MovieListFragment();
            fragmentManager.beginTransaction().replace(R.id.movies_list,listFragment, "ba/unsa/etf/rma/vj_20st/list").commit();
        }

        //slucaj kada mijenjamo orijentaciju uredaja iz portrait u landscape, a u aktivnosti je bio otvoren MovieDetailFragment
        //tada je potrebno skinuti MovieDetailFragment sa steka kako ne bi bio dodan na mjesto fragmenta MovieListFragment
        else fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    };

    @Override
    public void onItemClicked(Movie movie) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("movie", movie);
        MovieDetailFragment detailFragment = new MovieDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.movie_detail, detailFragment)
                    .commit();

        else getSupportFragmentManager().beginTransaction()
                    .replace(R.id.movies_list,detailFragment)
                    .addToBackStack(null).commit();

    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }
}
