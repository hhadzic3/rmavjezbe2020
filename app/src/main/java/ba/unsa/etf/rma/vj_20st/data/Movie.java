package ba.unsa.etf.rma.vj_20st.data;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class Movie implements Parcelable {
    private Integer id;
    private String title;
    private String genre;
    private String releaseDate;
    private String homepage;
    private String overview;
    private ArrayList<String> actors;
    private ArrayList<String> simelars;
    private String posterPath;

    public Movie(String title, String genre, String releaseDate, String homepage, String overview, ArrayList actors, ArrayList<String> simelars) {
        this.title = title;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.overview = overview;
        this.actors = actors;
        this.simelars = simelars;
    }

    protected Movie(Parcel in) {
        title = in.readString();
        genre = in.readString();
        releaseDate = in.readString();
        homepage = in.readString();
        overview = in.readString();
        actors = in.createStringArrayList();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public Movie(Integer id, String title, String overview, String releaseDate, String posterPath) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.overview = overview;
    }

    public Movie(String title, String overview, String releaseDate, String homepage, String posterPath, Integer id, String genre) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.overview = overview;
        this.homepage = homepage;
        this.genre = genre;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public ArrayList<String> getActors() {
        return actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public ArrayList<String> getSimelars() {
        return simelars;
    }

    public void setSimilarMovies(ArrayList<String> simelars) {
        this.simelars = simelars;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(homepage);
        dest.writeString(overview);
        dest.writeString(genre);
        dest.writeString(releaseDate);
        dest.writeArray(new ArrayList[]{actors});
        dest.writeArray(new ArrayList[]{simelars});
    }

}