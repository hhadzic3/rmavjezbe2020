package ba.unsa.etf.rma.vj_20st.list;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_20st.R;
import ba.unsa.etf.rma.vj_20st.data.Movie;


public class MovieListFragment extends Fragment implements IMovieListView{
    private OnItemClick oic;
    private ListView lv;
    private MyArrayAdapter movieListAdapter;
    public Button b;
    private EditText editText;

    private IMovieListPresenter movieListPresenter;

    public IMovieListPresenter getPresenter() {
        if (movieListPresenter == null) {
            movieListPresenter = new MovieListPresenter(this, getActivity());
        }
        return movieListPresenter;
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_list, container, false);
            lv = (ListView) view.findViewById(R.id.listView);
            editText = (EditText) view.findViewById(R.id.editText);
            b = (Button) view.findViewById(R.id.button);
            movieListAdapter = new MyArrayAdapter(getActivity(), R.layout.element_liste , new ArrayList<Movie>());
            lv.setAdapter(movieListAdapter);
        try {
            oic = (OnItemClick)getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }
        lv.setOnItemClickListener(listItemClickListener);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().searchMovies(editText.getText().toString());
            }
        });

        return view;
    }
    @Override
    public void showToast(String text){
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setMovies(ArrayList<Movie> movies) {
        movieListAdapter.setMovies(movies);
    }

    @Override
    public void notifyMovieListDataSetChanged() {
        movieListAdapter.notifyDataSetChanged();
    }


    public interface OnItemClick {
        void onItemClicked(Movie movie);
    }

    private AdapterView.OnItemClickListener listItemClickListener =
        new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie movie = movieListAdapter.getItem(position);
                oic.onItemClicked(movie);
            }
        };
}

