package ba.unsa.etf.rma.vj_20st.data;

import java.util.ArrayList;
import java.util.Arrays;

public final class MoviesModel {

    private static ArrayList<Movie> movies = new ArrayList<Movie>();

    static {
        ArrayList<String> glumci = new ArrayList();
        glumci.add("Tom Cruse");
        glumci.add("Brad Pit");
        glumci.add("Mustafa Nadarević");
        glumci.add("Goran Drago");
        glumci.add("Penelope Cruse");

        ArrayList<String> simelars = new ArrayList<String>(Arrays.asList("Reservoir Dogs", "Jackie Brown", "Pulp Fiction", "Memento", "Seven"));


        movies.add(new Movie("Focus", "Action", "14.10.2019.", "www.pulpfiction.com",
                "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.",
                glumci, simelars));
        movies.add(new Movie("The Lord of the Rings", "Fantasy", "02.11.1998", "www.lotr.com",
                "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to hi" , glumci, simelars));
        movies.add(new Movie("21 jump streer", "Comedy", "3.10.2018.", "www.pulpfiction.com",
                "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption."
        , glumci, simelars));

    }

    public static ArrayList<Movie> getMovies() {
        return movies;
    }
}
