package ba.unsa.etf.rma.vj_20st.detail;

import android.content.Context;
import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_20st.data.Movie;

public class MovieDetailPresenter {
    private Context context;
    private Movie movie;

    public MovieDetailPresenter(Context context) {
        this.context    = context;
    }
    public void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors,ArrayList<String> simelars) {
        this.movie = new Movie(title,overview,releaseDate,homepage,genre,actors, simelars);
    }
    public Movie getMovie() {
        return movie;
    }


    public void setMovie(Parcelable movie) {
        this.movie = (Movie)movie;
    }

    public void searchMovie(String query){
        new MovieDetailInteractor((MovieDetailInteractor.OnMovieSearchDone)
                this).execute(query);
    }

    public void onDone(Movie result) {
        movie = result;
        //view.refreshView();
    }

}
